import cv2
import numpy as np
from PIL import Image, ImageEnhance
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(24,GPIO.OUT)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.output(24,GPIO.HIGH)

camsource = cv2.VideoCapture(0)


state = GPIO.input(18)


def opencam():  
    cup= GPIO.input(17)
    cdwn = GPIO.input(27)
    while(True):

        ret1,img1 = camsource.read()

        if not ret1:
            print("no cams")
            break
        else:
            
            global enh 
            if(cup!=GPIO.input(17)):
                
                enh=enh+0.1
                cup=GPIO.input(17)
                print(enh)
            if(cdwn!=GPIO.input(27)):
                
                enh=enh-0.1
                cdwn=GPIO.input(27)
                print(enh)
  
            final= cv2.hconcat([img1,img1])
            
            contimg = Image.fromarray(final)
            en=ImageEnhance.Contrast(contimg)
            contimg = en.enhance(enh)
            imwhole = np.array(contimg)
               
            cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
            cv2.setWindowProperty("window",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
            cv2.imshow("window", imwhole)

            if cv2.waitKey(1) &  state!= GPIO.input(18):
                enh=1.45  
                break


  
    cv2.destroyAllWindows()
enh=1.45    
while True:
    if(state==GPIO.input(18)):
        opencam()
